class CLimeError(Exception):
    """Raise for errors"""
class NotEnoughSamplesCLimeError(CLimeError):
    """Raise when sampler is unable to retrieve required number of samples"""