import sys
from pysat.formula import CNFPlus
from pysat.card import CardEnc
import os


# Code for encoding arbitrary Boolean formulas into CNF, and for using CNF() instances of python-SAT directly
class TseitinEncoder():
	def __init__(self):
		self.formula = None
		self.maxVar = None
		self.binops = {'&':self.encodeAND,'|':self.encodeOR}
		self.unops = {'~':self.encodeNEG}
		self.clauses = None
		self.newAuxVarToUse = None
		self.onehots = None
		self.weights = None
		self.amoType = 0 #pairwise
		self.encoded = False

	def setConstraints(self, maxVar_, formula_= None, onehots_=None, amoType_ = 5, weights_ = None):
		if (formula_ == None or formula_==[]) and (onehots_ == None or onehots_==[]):
			print('Atleast one of formula or onehots cant be None or emptylist. Exiting..')
			sys.exit(1)
		self.formula = formula_
		self.maxVar = maxVar_
		self.clauses = CNFPlus()
		self.newAuxVarToUse = maxVar_ + 1
		self.amoType = amoType_
		self.encoded=False
		self.onehots = onehots_
	
	def getAuxVar(self):
		self.newAuxVarToUse += 1
		return self.newAuxVarToUse - 1

	def encodeAND(self, var1, var2, aux):
		newcls = []
		#aux => var1&var2 == (-aux | var1) & (-aux | var2)
		newcls.extend([[-aux,var1],[-aux,var2]])
		# var1&var2 => aux == -var1 | -var2 | aux
		newcls.extend([[-var1,-var2,aux]])
		return newcls
	
	def encodeOR(self, var1, var2, aux):
		newcls = []
		#aux => var1|var2 == (-aux | var1 | var2)
		newcls.extend([[-aux,var1,var2]])
		# var1|var2 => aux == (-var1 | aux) & (-var2 | aux)
		newcls.extend([[-var1,aux],[-var2,aux]])
		return newcls
	
	def encodeNEG(self, var, aux):
		newcls = []
		#aux => ~var
		newcls.extend([[-aux,-var]])
		#~var => aux
		newcls.extend([[var,aux]])
		return newcls

	def parseOperand(self, op):
		if type(op) is int:
			if op > self.maxVar:
				print('Error: var ID ',op,'is greater than maxVar',self.maxVar,'.Exiting..')
				sys.exit(1)
			elif op <= 0:
				print('Error: var ID cannot be <=0 (use ~ for negation) .Exiting..')
				sys.exit(1)
			else:
				var = op
		else:
			var = self.encodeSubFormula(op)	
		return var

	def encodeSubFormula(self, subF):
		if type(subF) is not list:
			print('Error in',str(subF),'Type of formula must be list. Exiting..')
			sys.exit(1)
		if len(subF) == 3:
			if subF[1] not in self.binops.keys():
				print('Error in',str(subF),'2nd element must be a binary operand (allowed:',str(self.binops),').Exiting..')
				sys.exit(1)
			var1 = self.parseOperand(subF[0])
			var2 = self.parseOperand(subF[2])
			currAuxVar = self.getAuxVar()
			self.clauses.extend(self.binops[subF[1]](var1,var2,currAuxVar))
			return currAuxVar

		elif len(subF) == 2:
			if subF[0] not in self.unops.keys():
				print('Error in',str(subF),'1st element in 2 element formula must be a unary operand (allowed:',str(self.unops.keys()),').Exiting..')
				sys.exit(1)
			var = self.parseOperand(subF[1])
			currAuxVar = self.getAuxVar()
			self.clauses.extend(self.unops[subF[0]](var,currAuxVar))
			return currAuxVar

		else:
			print("Error in ",str(subF),': Length of all subformulas must be 2 or 3. To encode a formula with a single literal x, use x|x or x&x. Exiting..')
			sys.exit(1)
	
	def encodeOneHots(self):
		if type(self.onehots) is not list:
			print("Onehots",str(self.onehots),'should be list of lists of integers. Exiting..')
			sys.exit(1)
		for hot in self.onehots:
			for h in hot:
				if (type(h) is not int) or h<=0 or h>self.maxVar:
					print("The onehot",str(hot),'contains',str(h),':either a non-integer or an integer <=0 or >maxVar. Exiting..')
					sys.exit(1)
			#at least one
			self.clauses.append(hot)
			#at most one 
			topvar = max(self.newAuxVarToUse-1,self.clauses.nv)
			amo = CardEnc.atmost(lits=hot,bound=1,top_id=topvar,encoding=self.amoType)
			self.clauses.extend(amo.clauses)

	def encodeAll(self):
		if self.maxVar == None:
			print('Error: Please setConstraints first')
			return
		print("Encoding all constraints.\nEncoding formula..")
		if self.formula != [] and self.formula != None:
			#force final literal to true (unit literal)
			self.clauses.append([self.encodeSubFormula(self.formula)])
		print("Encoding onehots..")
		if self.onehots != [] and self.onehots != None:
			self.encodeOneHots()
		print("Done encoding.")
		self.encoded = True

	def writeCNF(self, outputFileName, indSup=True):
		if self.encoded == False:
			self.encodeAll()
		cmt = 'c ind '
		for i in range(1,self.maxVar+1): cmt += str(i)+' '
		cmt += '0\n'
		if os.path.exists(outputFileName):
			os.remove(outputFileName)
		print('Writing CNF to file..')
		if indSup:
			self.clauses.to_file(outputFileName,[cmt])
		else:
			self.clauses.to_file(outputFileName)
		print('Done writing CNF!')

	def writeSomeCNF(self, f, outputFileName, maxVar, indSup=True, onehots_=None):
		assert (maxVar>0)
		print("Writing a preconstructed CNF (not encoded by this TseitinEncoder object)")
		cmt = 'c ind '
		for i in range(1,maxVar+1): cmt += str(i)+' '
		cmt += '0\n'
		if os.path.exists(outputFileName):
			os.remove(outputFileName)
		print('Writing CNF to file..')
		if indSup:
			f.to_file(outputFileName,[cmt])
		else:
			f.to_file(outputFileName)
		print('Done writing CNF!')