import os
from pysat.formula import CNFPlus
from pysat.solvers import Solver
from clime.exceptions import NotEnoughSamplesCLimeError
class Unigen():
	def __init__(self):
		pass
	def sample(self,inFileName, num_samples=5000,num_threads=None):
		outFolderName = os.environ['TMP']+'/unigen.samples'
		outFileName = outFolderName+'/'+inFileName.split('/')[-1][:-4]+'_0.txt'
		if num_threads==None:
			os.system('cd $UNIGEN; python $UNIGEN/UniGen2.py -runIndex=0 -samples='+str(num_samples)+' '+inFileName+' '+outFolderName)	
		else:
			os.system('cd $UNIGEN; python $UNIGEN/UniGen2.py -runIndex=0 -threads='+str(num_threads)+' -samples='+str(num_samples)+' '+inFileName+' '+outFolderName)
		print('Done Sampling! Retrieving samples from '+outFileName+' ..')
		samples = []
		f = open(outFileName,'r')
		lineno = 0
		for line in f:
			lineno += 1
			if lineno > num_samples:
				break
			samples.append([])
			vs = line.split()
			n = int(vs[-1].split(':')[1])
			vs = vs[:-1]
			vs[0] = vs[0][1:]
			i = 0
			for v in vs:
				v = int(v)
				i += 1
				assert (abs(v)==i)
				if v>0:
					samples[-1].append(1)
				else:
					samples[-1].append(0)
			for _ in range(n-1):
				samples.append(list(samples[-1]))
		return samples

class Unigen3():
	def __init__(self):
		pass
	def sample(self,inFileName, num_samples=5000,num_threads=None):
		outFileName = os.environ['TMP']+'/unigen3.samples'
		logFileName = os.environ['TMP']+'/unigen3.log'
		if os.path.exists(outFileName):
			os.remove(outFileName)
		if os.path.exists(logFileName):
			os.remove(logFileName)
		if num_threads==None:
			os.system('$UNIGEN3 --samples='+str(num_samples)+' --sampleout='+outFileName+' --input '+inFileName+' >'+logFileName+' 2>&1')	
		else:
			os.system('$UNIGEN3 --samples='+str(num_samples)+' --sampleout='+outFileName+' --th='+str(num_threads)+' --input '+inFileName+' >'+logFileName+' 2>&1')
		print('Done Sampling! Retrieving samples from '+outFileName+' ..')
		samples = []
		try:
			f = open(outFileName,'r')
		except OSError:
			print('Could not open sample file.')
			raise NotEnoughSamplesCLimeError
		lineno = 0
		for line in f:
			lineno += 1
			if lineno > num_samples:
				break
			samples.append([])
			vs = line.split()
			vs = vs[:-1]
			i = 0
			for v in vs:
				v = int(v)
				i += 1
				assert (abs(v)==i)
				if v>0:
					samples[-1].append(1)
				else:
					samples[-1].append(0)
		if len(samples) < num_samples:
			raise NotEnoughSamplesCLimeError
		return samples

class WAPS():
	#WAPS returns wrong samples https://github.com/meelgroup/WAPS/issues/2
	#do not use unless absolutely necessary
	#use processWaps instead of `withcheck once issue is resolved
	def __init__(self):
		pass
	def processWAPSSampleFile(self,cf_, num_samples):
		cnfmodels = []
		i = 1
		for line in cf_:
			ls = line.split()
			assert(ls[0]==str(i)+',')
			model = [0 if int(ls[j])==-j else 1 if int(ls[j])==j else None for j in range(1,len(ls))]
			cnfmodels.append(model)
			i += 1
		cf_.close()
		if i<=num_samples:
			print('Samples requested:',num_samples,'Samples got:',i-1)
			raise NotEnoughSamplesCLimeError
		return cnfmodels

	def processWAPSSampleFileWithCheck(self,cf_, cnf, num_samples):
		cnfmodels = []
		i = 1
		for line in cf_:
			ls = line.split()
			assert(ls[0]==str(i)+',')
			
			g=cnf.copy()
			for j in range(1,len(ls)):
				g.append([int(ls[j])])
			with Solver(name='m22', bootstrap_with=g) as oracle:
				if not oracle.solve():
					print('WAPS gave wrong sample (sample #',i,')')
					raise NotEnoughSamplesCLimeError
				
			model = [0 if int(ls[j])==-j else 1 if int(ls[j])==j else None for j in range(1,len(ls))]
			cnfmodels.append(model)
			i += 1
		cf_.close()
		if i<=num_samples:
			print('Samples requested:',num_samples,'Samples got:',i-1)
			raise NotEnoughSamplesCLimeError
		return cnfmodels
		

	def sample(self,inFileName, num_samples=5000):
		try:
			outFileName = os.environ['TMP']+'/waps.samples'
			if os.path.exists(outFileName):
				os.remove(outFileName)
			os.system('python $WAPS/waps.py --outputfile='+outFileName+' --samples='+str(num_samples)+' '+inFileName)
			cnf = CNFPlus(from_file=inFileName)	
			print('Done Sampling! Retrieving samples from '+outFileName+' ..')
			f = open(outFileName,'r')
		except OSError:
			print('Could not sample')
			raise NotEnoughSamplesCLimeError
		return self.processWAPSSampleFileWithCheck(f, cnf, num_samples)