import math

class BaseEstimator(object):
	"""
	 * Adapted from MayBMS implementation of AA algorithm in source/src/backend/maybms/aconf.c 
	 * 
	 * 
	 * Algorithm modified from MayBMS to use only two sets of random variables Z_i and Z_i' as defined in the latest version
	 * of the paper https://epubs.siam.org/doi/abs/10.1137/S0097539797315306?journalCode=smjcat.
	 * In particular, the results S_sra and N_sra from Step 1 (Stopping Rule algorithm) are used again in Step 3.
	 * Lines with the comment 'Sample Once Modification' are the ones added / changed from the original MayBMS implementation.
	 * 
	 * Another modification is enabling batch sampling (maybe faster in some cases) in the call to generateSamples
	 *
	"""
	
	def __init__(self, approxSampler_ = False, verb=0):
		self.numerator = -1
		self. denominator = 1
		self.approxSampler = approxSampler_
		self.verb = verb
	
	def computeEstimate(self, eps_approx=0.8, eps_thresh=0.1, delta=0.2, gamma=0.05):
		eps1 = None; eps_Sampler=None; eta=None
		epsilon = eps_thresh
		if self.approxSampler:
			eps_Sampler = 0.5 * epsilon
			term1 = epsilon + epsilon*epsilon/2 - gamma*epsilon/2
			term2 = (epsilon -  gamma*epsilon/2)/(1+epsilon/2)
			eta = min(term1,term2)
		else:
			eps_Sampler = 0
			eta = epsilon
		
		if self.checkThreshold(eps_Sampler,eta, delta, gamma):
			return self.numerator/self.denominator
		epsilon = eps_approx 
		if self.approxSampler:
			eps1 = 0.4 * epsilon
			eps_Sampler = 0.4 * epsilon
		else:
			eps1 = epsilon
			eps_Sampler = 0
		self.AAP(eps1,eps_Sampler, delta)
		return self.numerator/self.denominator
		
	def checkThreshold(self, eps_Sampler, eta, delta, gamma):	
		N = math.log(1/delta)/(2*eta*eta)+1
		samples = self.generateAndCheckSamples(int(math.ceil(N)),eps_Sampler)
		assert(len(samples) >= N)
		S = 0
		for smpl in samples:
			S += smpl
		C = S/N
		if C <= gamma:
			return True
		else:
			return False
			
	def generateAndCheckSamples(self, n_samples, eps_2):
		raise NotImplementedError()

	def AAP(self, eps1, eps_Sampler, delta):
		e = 2.718281828459
		upsilon  = 4.0 * (e - 2.0) * math.log(2 / delta) / (eps1 * eps1)
		upsilon2 = 2.0 * (1.0 + math.sqrt(eps1)) * (1.0 + 2.0 * math.sqrt(eps1))  * (1.0 + math.log(1.5)/math.log(2/delta)) * upsilon
							
		mu_hat = None
		rho_hat = None

		N_sra = 0                                          # SAMPLE_ONCE_MODIFICATION
		S_sra = 0											# SAMPLE_ONCE_MODIFICATION

		#/* Step 1: stopping rule algorithm */
		#epsilon_sra = 0.5 if (0.5 < math.sqrt(eps1)) else math.sqrt(eps1)
		epsilon_sra = min(0.5, math.sqrt(eps1))
		delta_sra = delta/3
		upsilon_sra = 1.0 + (1.0 + epsilon_sra) * upsilon
		if self.verb != 0:
			print("Starting stopping rule. Upsilon = ",upsilon_sra)
		N = 0
		S = 0
		num_sample_request = max(1.0,upsilon_sra - S)
		while(S < upsilon_sra):
			samples = self.generateAndCheckSamples(int(math.ceil(num_sample_request)), eps_Sampler)
			N += len(samples)
			for smpl in samples:
				S += smpl
			num_sample_request = max(1.0, upsilon_sra - S)
			S_sra = S											 # SAMPLE_ONCE_MODIFICATION
			
		mu_hat = upsilon_sra / float(N)
		N_sra = N												 # SAMPLE_ONCE_MODIFICATION
		
		if self.verb != 0:
			print("Finished stopping rule. MU:",mu_hat)

		#/* Step 2: */
		N = math.ceil(upsilon2 * eps1 / mu_hat)
		S = 0
		i = None
		if self.verb != 0:
			print("Estimating rho. N = ",N)
		samples = self.generateAndCheckSamples(int(math.ceil(2*N)),eps_Sampler)
		assert(len(samples) >= 2*N)
		for i in range(1,N+1):
			kl2 = samples[2*(i-1)] - samples[2*i-1]
			S += kl2 * kl2 / 2.0

		#rho_hat = (S / N) if (S / N > eps1 * mu_hat) else (eps1 * mu_hat)
		rho_hat = max(S/N, eps1 * mu_hat)
		if self.verb != 0:
			print("Done estimatingRho. S / N = ", (S/N), " ..epsilon*mu_hat = ",eps1*mu_hat," ..Rho:",rho_hat)
		
		#/* Step 3: */
		N = upsilon2 * rho_hat / (mu_hat * mu_hat) + 1
		if self.verb != 0:
			print("Den(N) (no. of samples to generate for final count):",N)
		if self.verb != 0:
			print("N_sra = ",N_sra," S_sra = ",S_sra)					#// SAMPLE_ONCE_MODIFICATION
		if N_sra >= N:												#// SAMPLE_ONCE_MODIFICATION
			self.numerator = S_sra									#// SAMPLE_ONCE_MODIFICATION
			self.denominator = N_sra								#// SAMPLE_ONCE_MODIFICATION
		else:
			S = S_sra										#// SAMPLE_ONCE_MODIFICATION
			samples = self.generateAndCheckSamples(int(math.ceil(N-N_sra)),eps_Sampler)
			assert(len(samples) >= (N-N_sra))
			for smpl in samples:
				S += smpl
			self.numerator = S
			self.denominator = N_sra + len(samples)
		self.denominator = math.ceil(self.denominator)
		assert (self.numerator <= self.denominator)
		if self.verb != 0:
			print("Num:",self.numerator," Den:",self.denominator)
