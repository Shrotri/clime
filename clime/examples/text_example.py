import clime
import sklearn
import numpy as np
import sklearn
import sklearn.ensemble
import sklearn.metrics
from clime.clime_text import IndexedString
#from __future__ import print_function
import sys
from pysat.formula import CNFPlus
from pysat.card import CardEnc

if sys.argv[1]=='1':
	bowIn = True
else:
	bowIn = False
print('Input argument set bow to ',bowIn)
from sklearn.datasets import fetch_20newsgroups
categories = ['alt.atheism', 'soc.religion.christian']
newsgroups_train = fetch_20newsgroups(subset='train', categories=categories)
newsgroups_test = fetch_20newsgroups(subset='test', categories=categories)
class_names = ['atheism', 'christian']

vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(lowercase=False)
train_vectors = vectorizer.fit_transform(newsgroups_train.data)
test_vectors = vectorizer.transform(newsgroups_test.data)

rf = sklearn.ensemble.RandomForestClassifier(n_estimators=500)
rf.fit(train_vectors, newsgroups_train.target)

pred = rf.predict(test_vectors)
sklearn.metrics.f1_score(newsgroups_test.target, pred, average='binary')

from clime import clime_text
from sklearn.pipeline import make_pipeline
c = make_pipeline(vectorizer, rf)

print(c.predict_proba([newsgroups_test.data[0]]))

from clime.clime_text import CLimeTextExplainer
explainer = CLimeTextExplainer(class_names=class_names, bow=bowIn)

idx = 83
exp = explainer.explain_instance(newsgroups_test.data[idx], c.predict_proba, num_features=6)
print('Document id: %d' % idx)
print('Probability(christian) =', c.predict_proba([newsgroups_test.data[idx]])[0,1])
print('True class: %s' % class_names[newsgroups_test.target[idx]])

print(exp.as_list())

print('Original prediction:', rf.predict_proba(test_vectors[idx])[0,1])
tmp = test_vectors[idx].copy()
tmp[0,vectorizer.vocabulary_['Posting']] = 0
tmp[0,vectorizer.vocabulary_['Host']] = 0
print('Prediction removing some features:', rf.predict_proba(tmp)[0,1])
print('Difference:', rf.predict_proba(tmp)[0,1] - rf.predict_proba(test_vectors[idx])[0,1])

print('=========================================================================')
print("Doing with constraints..")
print('=========================================================================')

idx = 83
bowIn = True
print('BOW set to true for this test.')
explainer = CLimeTextExplainer(class_names=class_names, bow=bowIn)
ind_str = IndexedString( newsgroups_test.data[idx], bow=bowIn)
cons = [ [ [[('unm',), '&', ('edu',)],'|', [('jchadwic',),'|', ('Subject',)]] ,'&', [[('Another',),'|',('request',)],'&', [('for',),'|', ['~',('Darwin',)]]] ],'&', [ [('Fish',),'|', ('Organization',)],'&', [ [['~',('University',)],'|', ('New',)],'&', ['~',('Mexico',)] ] ] ]
consType = 0
exp = explainer.explain_instance_with_constraints(ind_str, c.predict_proba, num_features=6, consLst=cons,consType=consType)
print('Document id: %d' % idx)
print('Probability(christian) =', c.predict_proba([newsgroups_test.data[idx]])[0,1])
print('True class: %s' % class_names[newsgroups_test.target[idx]])

print(exp.as_list())

print()
print('=========================================================================')
print()

idx = 83
bowIn = False
print('BOW set to False for this test.')
explainer = CLimeTextExplainer(class_names=class_names, bow=bowIn)
ind_str = IndexedString( newsgroups_test.data[idx], bow=bowIn)
cons = [ [ [[('unm',1), '&', ('edu',2)],'|', [('jchadwic',0),'|', ('Subject',0)]] ,'&', [[('Another',0),'|',('request',0)],'&', [('for',0),'|', ['~',('Darwin',0)]]] ],'&', [ [('Fish',0),'|', ('Organization',0)],'&', [ [['~',('University',0)],'|', ('New',0)],'&', ['~',('Mexico',0)] ] ] ]
consType = 1
exp = explainer.explain_instance_with_constraints(ind_str, c.predict_proba, num_features=6, consLst=cons,consType=consType)
print('Document id: %d' % idx)
print('Probability(christian) =', c.predict_proba([newsgroups_test.data[idx]])[0,1])
print('True class: %s' % class_names[newsgroups_test.target[idx]])

print(exp.as_list())

print()
print('=========================================================================')
print()

idx = 83
bowIn = False
print('Testing with custom pysat formula. BOW set to False for this test.')
explainer = CLimeTextExplainer(class_names=class_names, bow=bowIn)
ind_str = IndexedString( newsgroups_test.data[idx], bow=bowIn)
cons = CNFPlus()
#set total number of non-aux variables to number of words in inverse_vocab i.e. doc_size in clime_text.py
cons.nv = len(ind_str.inverse_vocab)
print('Number of words in inverse_vocab:',cons.nv)
#add constraint making atleast 3 of first 5 variables true
#top_id parameter ensures that there is no clash between original vars and aux vars generated during encoding of cardinality constraints
cons.extend(CardEnc.atleast(lits=[1, 2, 3, 4, 5], bound=3,top_id=cons.nv).clauses)
consType = 2
cons.to_file('test.cnf')

exp = explainer.explain_instance_with_constraints(ind_str, c.predict_proba, num_features=6, consLst=cons,consType=consType)
print('Document id: %d' % idx)
print('Probability(christian) =', c.predict_proba([newsgroups_test.data[idx]])[0,1])
print('True class: %s' % class_names[newsgroups_test.target[idx]])

print(exp.as_list())


#at most one 
# topvarF = f.nv
# topvarNF = nf.nv
# amof = CardEnc.atmost(lits=hot,bound=1,top_id=topvarF,encoding=5)
# amonf = CardEnc.atmost(lits=hot,bound=1,top_id=topvarNF,encoding=5)
# f.extend(amof.clauses)
# nf.extend(amonf.clauses)
