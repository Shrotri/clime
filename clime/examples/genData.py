import sys,os, random

numSamples = int(sys.argv[1])
numFeatures = int(sys.argv[2])
lo = int(sys.argv[3])
hi = int(sys.argv[4])
thresh = int(sys.argv[5])
threshcnt = int(sys.argv[6])

ofname = 'data.csv'

of = open(ofname,'w')
for i in range(numSamples):
	rns = [None]*numFeatures
	for j in range(numFeatures):
		rns[j] = random.randint(lo,hi)
	cnt = 0
	for rn in rns:
		if rn>=thresh:
			cnt += 1
	if cnt >= threshcnt:
		of.write(str(1)+', ')
	else:
		of.write('0, ')
	for j in range(numFeatures-1):
		of.write(str(rns[j])+', ')
	of.write(str(rns[numFeatures-1])+'\n')
of.close()