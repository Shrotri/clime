import numpy as np
import matplotlib.pyplot as plt
from skimage.color import gray2rgb, rgb2gray, label2rgb # since the code wants color images

from sklearn.datasets import fetch_openml
mnist = fetch_openml('mnist_784',as_frame=False)
# make each image color so lime_image works correctly
X_vec = np.stack([gray2rgb(iimg) for iimg in mnist.data.reshape((-1, 28, 28))],0)
y_vec = mnist.target.astype(np.uint8)

#%matplotlib inline
# fig, ax1 = plt.subplots(1,1)
# ax1.imshow(X_vec[0], interpolation = 'none')
# ax1.set_title('Digit: {}'.format(y_vec[0]))
# plt.show()

from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import Normalizer

from pysat.formula import CNFPlus
from pysat.card import CardEnc


class PipeStep(object):
    """
    Wrapper for turning functions into pipeline transforms (no-fitting)
    """
    def __init__(self, step_func):
        self._step_func=step_func
    def fit(self,*args):
        return self
    def transform(self,X):
        return self._step_func(X)


makegray_step = PipeStep(lambda img_list: [rgb2gray(img) for img in img_list])
flatten_step = PipeStep(lambda img_list: [img.ravel() for img in img_list])

simple_rf_pipeline = Pipeline([
    ('Make Gray', makegray_step),
    ('Flatten Image', flatten_step),
    #('Normalize', Normalizer()),
    #('PCA', PCA(16)),
    ('RF', RandomForestClassifier())
                              ])

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X_vec, y_vec,
                                                    train_size=0.55)

simple_rf_pipeline.fit(X_train, y_train)

#%load_ext autoreload
#%autoreload 2
import os,sys
import clime
from clime import clime_image
from clime.wrappers.scikit_image import SegmentationAlgorithm
explainer = clime_image.CLimeImageExplainer(verbose = False, sampler='unigen')
segmenter = SegmentationAlgorithm('quickshift', kernel_size=1, max_dist=200, ratio=0.2)

'''
#%%time
explanation = explainer.explain_instance(X_test[0], 
                                         classifier_fn = simple_rf_pipeline.predict_proba, 
                                         top_labels=10, hide_color=0, num_samples=10000, segmentation_fn=segmenter)

temp, mask = explanation.get_image_and_mask(y_test[0], positive_only=True, num_features=10, hide_rest=False, min_weight = 0.01)
fig, (ax1, ax2) = plt.subplots(1,2, figsize = (8, 4))
ax1.imshow(label2rgb(mask,temp, bg_label = 0), interpolation = 'nearest')
ax1.set_title('Positive Regions for {}'.format(y_test[0]))
#plt.show()
temp, mask = explanation.get_image_and_mask(y_test[0], positive_only=False, num_features=10, hide_rest=False, min_weight = 0.01)
ax2.imshow(label2rgb(3-mask,temp, bg_label = 0), interpolation = 'nearest')
ax2.set_title('Positive/Negative Regions for {}'.format(y_test[0]))
plt.show()

#Select the same class explained on the figures above.
ind =  explanation.top_labels[0]
#Map each explanation weight to the corresponding superpixel
dict_heatmap = dict(explanation.local_exp[ind])
heatmap = np.vectorize(dict_heatmap.get)(explanation.segments) 
#Plot. The visualization makes more sense if a symmetrical colorbar is used.
plt.imshow(heatmap, cmap = 'RdBu', vmin  = -heatmap.max(), vmax = heatmap.max())
plt.colorbar()
plt.show()
'''

# # now show them for each class
# fig, m_axs = plt.subplots(2,5, figsize = (12,6))
# for i, c_ax in enumerate(m_axs.flatten()):
#     temp, mask = explanation.get_image_and_mask(i, positive_only=True, num_features=1000, hide_rest=False, min_weight = 0.01 )
#     c_ax.imshow(label2rgb(mask,X_test[0], bg_label = 0), interpolation = 'nearest')
#     c_ax.set_title('Positive for {}\nActual {}'.format(i, y_test[0]))
#     c_ax.axis('off')
# plt.show()

# pipe_pred_test = simple_rf_pipeline.predict(X_test)
# wrong_idx = np.random.choice(np.where(pipe_pred_test!=y_test)[0])
# print('Using #{} where the label was {} and the pipeline predicted {}'.format(wrong_idx, y_test[wrong_idx], pipe_pred_test[wrong_idx]))

# #%%time
# explanation = explainer.explain_instance(X_test[wrong_idx], 
#                                          classifier_fn = simple_rf_pipeline.predict_proba, 
#                                          top_labels=10, hide_color=0, num_samples=10000, segmentation_fn=segmenter)

# # now show them for each class
# fig, m_axs = plt.subplots(2,5, figsize = (12,6))
# for i, c_ax in enumerate(m_axs.flatten()):
#     temp, mask = explanation.get_image_and_mask(i, positive_only=True, num_features=10, hide_rest=False, min_weight = 0.01 )
#     c_ax.imshow(label2rgb(mask,temp, bg_label = 0), interpolation = 'nearest')
#     c_ax.set_title('Positive for {}\nActual {}'.format(i, y_test[wrong_idx]))
#     c_ax.axis('off')
# plt.show()

idx = 20
img = X_test[idx]
res = y_test[idx]
fig, ax1 = plt.subplots(1,1)
ax1.imshow(img, interpolation = 'none')
ax1.set_title('Digit: {}'.format(res))
plt.show()

explanation = explainer.explain_instance(img, 
                                         classifier_fn = simple_rf_pipeline.predict_proba, 
                                         top_labels=10, hide_color=0, num_samples=10000, segmentation_fn=segmenter)
#Select the same class explained on the figures above.
ind =  explanation.top_labels[0]
#Map each explanation weight to the corresponding superpixel
dict_heatmap = dict(explanation.local_exp[ind])
heatmap = np.vectorize(dict_heatmap.get)(explanation.segments) 
#Plot. The visualization makes more sense if a symmetrical colorbar is used.
plt.imshow(heatmap, cmap = 'RdBu', vmin  = -heatmap.max(), vmax = heatmap.max())
plt.colorbar()
plt.show()
plt.savefig('lime_exp')

segments = segmenter(img)
nVars = np.unique(segments).shape[0]
cons = CNFPlus()
#set total number of non-aux variables to number of words in inverse_vocab i.e. doc_size in clime_text.py
cons.nv = nVars
print('Number of unique segments:',cons.nv)
#add constraint making atleast 3 of first 5 variables true
#top_id parameter ensures that there is no clash between original vars and aux vars generated during encoding of cardinality constraints
cons.extend(CardEnc.atleast(lits=list(range(1,nVars)), bound=int(3*nVars/4),top_id=cons.nv).clauses)

cons.to_file('test.cnf')

explanation = explainer.explain_instance_with_constraints(img,simple_rf_pipeline.predict_proba,segments, top_labels=10,
                                                          hide_color=0, num_samples=10000,consLst = cons)
#Select the same class explained on the figures above.
ind =  explanation.top_labels[0]
#Map each explanation weight to the corresponding superpixel
dict_heatmap = dict(explanation.local_exp[ind])
heatmap = np.vectorize(dict_heatmap.get)(explanation.segments) 
#Plot. The visualization makes more sense if a symmetrical colorbar is used.
plt.imshow(heatmap, cmap = 'RdBu', vmin  = -heatmap.max(), vmax = heatmap.max())
plt.colorbar()
plt.show()
plt.savefig('clime_exp')
