from __future__ import print_function
import sklearn
import sklearn.datasets
import sklearn.ensemble
from sklearn.compose import ColumnTransformer
import numpy as np
#import clime
import clime.clime_tabular
np.random.seed(1)
from matplotlib import pyplot as plt
import random, sys

usecons = int(sys.argv[1])

data = np.genfromtxt('data.csv', delimiter=', ', dtype=str)
nfeatures = data.shape[1]-1
nsamples = data.shape[0]
feature_names = ['f'+str(i) for i in range(1,nfeatures+1)]
labels = data[:,0]
le= sklearn.preprocessing.LabelEncoder()
le.fit(labels)
labels = le.transform(labels)
class_names = le.classes_
data = data[:,1:]

categorical_features = [i for i in range(nfeatures)]

categorical_names = {}
for feature in categorical_features:
    le = sklearn.preprocessing.LabelEncoder()
    le.fit(data[:, feature])
    data[:, feature] = le.transform(data[:, feature])
    categorical_names[feature] = le.classes_

data = data.astype(float)

encoder = ColumnTransformer([("Toy", sklearn.preprocessing.OneHotEncoder(), categorical_features)], remainder = 'passthrough')
np.random.seed(1)
train, test, labels_train, labels_test = sklearn.model_selection.train_test_split(data, labels, train_size=0.80)
encoder.fit(data)
encoded_train = encoder.transform(train)

print("Done transforming data!\n")

# import xgboost
# gbtree = xgboost.XGBClassifier(n_estimators=300, max_depth=5)
# print("Training model..\n")
# gbtree.fit(encoded_train, labels_train)
# print("Model accuracy is :",sklearn.metrics.accuracy_score(labels_test, gbtree.predict(encoder.transform(test)),normalize=True))

from sklearn.neural_network import MLPClassifier

mlp = MLPClassifier(hidden_layer_sizes=(8,8,8), activation='relu', solver='adam', max_iter=1000)
mlp.fit(encoded_train,labels_train)
#mlp.fit(train,labels_train)

print("Model accuracy is :",sklearn.metrics.accuracy_score(labels_test, mlp.predict(encoder.transform(test)),normalize=True))
#print("Model accuracy is :",sklearn.metrics.accuracy_score(labels_test, mlp.predict(test),normalize=True))
print("Starting lime..\n")

predict_fn = lambda x: mlp.predict_proba(encoder.transform(x)).astype(float)
#predict_fn = lambda x: mlp.predict_proba(x).astype(float)

explainer = clime.clime_tabular.CLimeTabularExplainer(train ,feature_names = feature_names,class_names=class_names,
                                                   categorical_features=categorical_features
                                                   #categorical_names=categorical_names 
                                                   )

print("Lime finished!\n")
np.random.seed(1)

#Original test set
'''
ntest = test.shape[1]
print (test.shape)
'''

newTest = [3,3,3,5,5,5,5,1,1,1]
test = np.array(newTest)
if usecons==0:
	exp = explainer.explain_instance(test, predict_fn, num_features=10)
elif usecons==1:
	cons1 = [[(7,1),'&',(8,1)],'&',(9,1)]
	exp = explainer.explain_instance_with_constraints(test, predict_fn, num_features=10, consLst=cons1)
else:
	cons2=[[[(7,1),'|',[(7,2),'|',(7,3)]], '&',[(8,1),'|',[(8,2),'|',(8,3)]]],'&',[(9,1),'|',[(9,2),'|',(9,3)]]]
	exp = explainer.explain_instance_with_constraints(test, predict_fn, num_features=10, consLst=cons2)
exp.as_pyplot_figure()
plt.show()

#import warnings
#from lime import submodular_pick
#sp_obj = submodular_pick.SubmodularPick(explainer, test, predict_fn, sample_size=10, num_features=5, num_exps_desired=10)
#for exp in sp_obj.sp_explanations:
#    exp.as_pyplot_figure()
#    plt.show() 

'''
for i in range(10):
    j = random.randint(0,ntest-1)
    exp = explainer.explain_instance(test[j], predict_fn, num_features=10)
    #exp.show_in_notebook(show_all=False)
    #exp.save_to_file('exp_credit_20.html')
    print(exp.available_labels())
    exp.as_pyplot_figure()
    plt.show()
exp.as_pyplot_figure().savefig('exp.png')
'''
