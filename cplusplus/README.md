## Introduction

This directory contains CPP code for 
- the 'computeEstimate' procedure in the CLIME paper for certifying explanations using constrained sampling (see Estimator.h and Estimator.cpp)
- specializing computeEstimate for operating on CNF benchmarks of [Narodytska et al. '19] 

## Details

This is a C++ implementation meant for comparing the scalability of computeEstimate vs. the technique of [Narodytska et al., '19] that uses ApproxMC, and is therefore specialized for CNF benchmarks used therein. The files Estimator.cpp and Estimator.h contain the code for computeEstimate, and can be easily extended for use in custom applications. The Python implementation of computeEstimate is in the file clime/wrappers/base_estimator.py (from root directory of the repository). 



## Requirements

Install [Cryptominisat5](https://github.com/msoos/cryptominisat) and set environment variable $CRYPTOMS to the path of the 'build/' directory of Cryptominisat5.

## Compile
In this directory run
```sh
mkdir objs
make
```
This will create the binary 'computeCNFEstimate'
## Usage

```sh
./computeCNFEstimate <eps_approx> <eps_thresh> <delta> <gamma> <expName> <input_cnf_dimacs_file>
```
eps_approx, eps_thresh - tolerances (see CLIME paper)  
delta - confidence
gamma - threshold
expName - experiment (adult, recid, lend, -)  
input_cnf_dimacs_file - the sample.cnf file for the given instance. 

e.g.: 
```
./computeCNFEstimate 0.8 0.1 0.2 0.05 sample.cnf
```