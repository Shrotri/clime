#ifndef BBSCNFESTIMATOR_H_
#define BBSCNFESTIMATOR_H_

#include "Estimator.h"
#include "BoolBoundarySampler.h"
#include "CNFChecker.h"
#include "CNFFormula.h"

using std::vector;
class BBSCNFEstimator: public Estimator{
	public:
		BBSCNFEstimator(CNFFormula F_, std::string expName_):
		Estimator(false), Checker(F_),BBS(F_,expName_){}
					
	protected:
		vector<double_t> generateAndCheckSamples(uint64_t n_samples, double_t eps_2) override;
		BoolBoundarySampler BBS;
		CNFChecker Checker;
};

#endif
