#include <cassert>

#include "Estimator.h"

using std::cout;
using std::endl;
using std::min;

/* Adapted from MayBMS implementation of AA algorithm in source/src/backend/maybms/aconf.c 
 * 
 * 
 * Algorithm modified from MayBMS to use only two sets of random variables Z_i and Z_i' as defined in the latest version
 * of the paper https://epubs.siam.org/doi/abs/10.1137/S0097539797315306?journalCode=smjcat.
 * In particular, the results S_sra and N_sra from Step 1 (Stopping Rule algorithm) are used again in Step 3.
 * Lines with the comment 'Sample Once Modification' are the ones added / changed from the original MayBMS implementation.
 * 
 * Another modification is enabling batch sampling (maybe faster in some cases) in the call to generateSamples
 */

Estimate& Estimator::computeEstimate(double_t eps_approx, double_t eps_thresh, double_t delta, double_t gamma){
	double_t eps1, eps_Sampler, eta;
	double_t epsilon = eps_thresh;
	if (approxSampler){
		eps_Sampler = 0.5 * epsilon;
		double_t term1 = epsilon + epsilon*epsilon/2 - gamma*epsilon/2;
		double_t term2 = (epsilon -  gamma*epsilon/2)/(1+epsilon/2);
		eta = min(term1,term2);
	} else{
		eps_Sampler = 0;
		eta = epsilon;
	}
	if (checkThreshold(eps_Sampler,eta, delta, gamma)){
		return count;
	}
	epsilon = eps_approx; 
	if (approxSampler){
		eps1 = 0.4 * epsilon;
		eps_Sampler = 0.4 * epsilon;
	} else{
		eps1 = epsilon;
		eps_Sampler = 0;
	}
	AAP(eps1,eps_Sampler, delta);
	return count;
}

bool Estimator::checkThreshold(double_t eps_Sampler, double_t eta, double_t delta, double_t gamma){
	uint64_t N = log(1/delta)/(2*eta*eta)+1;
	vector<double_t> samples = generateAndCheckSamples(N,eps_Sampler);
	assert(samples.size() >= N);
	double_t S = 0;
	for (auto& smpl : samples)
		S += smpl;
	double_t C = S/N;
	if (C <= gamma){
		return true;
	} else{
		return false;
	}
}

int Estimator::AAP(double_t eps1, double_t eps_Sampler, double_t delta){
	const double_t e = 2.718281828459;

   	const double_t upsilon  = 4.0 * (e - 2.0) * log(2 / delta)
                            / (eps1 * eps1);

   	const double_t upsilon2 = 2.0 * (1.0 + sqrt(eps1))
                         * (1.0 + 2.0 * sqrt(eps1))
                         * (1.0 + log(1.5)/log(2/delta)) * upsilon;

	double_t mu_hat;
 	double_t rho_hat;

	uint64_t N_sra = 0;                                          // SAMPLE_ONCE_MODIFICATION
	double_t S_sra = 0;											 // SAMPLE_ONCE_MODIFICATION
   	/* Step 1: stopping rule algorithm */
	{
		const double_t epsilon_sra =
			(0.5 < sqrt(eps1)) ? 0.5 : sqrt(eps1);
		const double_t delta_sra = delta/3;
		const double_t upsilon_sra = 1.0 + (1.0 + epsilon_sra)
		                        * upsilon;
		cout<<"Starting stopping rule. Upsilon = "<<upsilon_sra<<endl;
		uint64_t N = 0;
		double_t S = 0;
		uint64_t num_sample_request = std::max(1.0,upsilon_sra - S);
		while(S < upsilon_sra)
		{
		   vector<double_t> samples = generateAndCheckSamples(num_sample_request, eps_Sampler);
		   N += samples.size();
		   for (auto& smpl : samples)
    	      S += smpl;
		   num_sample_request = std::max(1.0, upsilon_sra - S);	
		   S_sra = S;											 // SAMPLE_ONCE_MODIFICATION
		}
		
		mu_hat = upsilon_sra / (double_t)N;
		N_sra = N;												 // SAMPLE_ONCE_MODIFICATION
	}
	cout<<"Finished stopping rule. MU:"<<mu_hat<<endl;
   	/* Step 2: */
	{
   		const uint64_t N = ceil(upsilon2 * eps1 / mu_hat);
 	  	double_t S = 0;
		uint64_t i;
		cout<<"Estimating rho. N = "<<N<<endl;
		vector<double_t> samples = generateAndCheckSamples(2*N,eps_Sampler);
		assert(samples.size() >= 2*N);
   		for(i = 1; i <= N; i++)
   		{
      		   double_t kl2 = samples[2*(i-1)]
		            - samples[2*i-1];
      		   S += kl2 * kl2 / 2.0;
   		}
   
 	  	rho_hat = (S / N > eps1 * mu_hat) ?
			  (S / N) : (eps1 * mu_hat);
		cout<<"Done estimatingRho. S / N = "<< (S/N)<< " ..epsilon*mu_hat = "<<eps1*mu_hat<<" ..Rho:"<<rho_hat<<endl;
	}
	
   	/* Step 3: */
	const uint64_t N = upsilon2 * rho_hat / (mu_hat * mu_hat) + 1;
	cout<<"Den(N) (no. of samples to generate for final count):"<<N<<endl;
	cout<<"N_sra = "<<N_sra<<" S_sra = "<<S_sra<<endl;			// SAMPLE_ONCE_MODIFICATION
	if (N_sra >= N){											// SAMPLE_ONCE_MODIFICATION
		count.num = S_sra;										// SAMPLE_ONCE_MODIFICATION
		count.den = N_sra;										// SAMPLE_ONCE_MODIFICATION
	} else{
		double_t S = S_sra;										// SAMPLE_ONCE_MODIFICATION
		vector<double_t> samples = generateAndCheckSamples(N-N_sra,eps_Sampler);
		assert(samples.size() >= (N-N_sra));
		for(auto& smpl: samples)
			S += smpl;
		cout<<"Num:"<<S<<endl;
		count.num = S;
		count.den = N;
	}
	return (1);
}
