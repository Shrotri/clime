#ifndef CNFCHECKER_H_
#define CNFCHECKER_H_

#include "CNFFormula.h"
#include <cryptominisat5/cryptominisat.h>

using std::vector;
class CNFChecker{
	public:
		CNFChecker(CNFFormula F_):F(F_){
			if(!initCMS()){
				std::cout<<"Could not initialize CryptoMiniSAT. Exiting..\n";
				exit(1);
			}
		}
		bool checkSample(vector<bool> s);
	protected:
		bool initCMS();
		CMSat::SATSolver solver;
		CNFFormula F;
		CMSat::lbool posRes;
};

#endif
