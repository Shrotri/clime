#ifndef BOOLBOUNDARYSAMPLER_H_
#define BOOLBOUNDARYSAMPLER_H_

#include <ctime>
#include <iostream>
#include <cassert>

#include "CNFFormula.h"
#include "RandomBits.h"

using std::vector;
using std::string;
class BoolBoundarySampler{
	public:
		BoolBoundarySampler(CNFFormula F_, string expName):
		F(F_){
			std::pair<vector<uint64_t>,vector<uint64_t>> adult({{4,9,7,4,9,6,5,2,3,3,3,11},{0,4,13,20,24,33,39,44,46,49,52,55,66}});
			std::pair<vector<uint64_t>,vector<uint64_t>> recid({2,2,2,2,2,2,2,2,2,2,4,4,3,4,4},{0,2,4,6,8,10,12,14,16,18,20,24,28,31,35,39});
			std::pair<vector<uint64_t>,vector<uint64_t>> lend({4,4,4,4,3,4,4,4,4},{0,4,8,12,16,19,23,27,31,35});

			if (expName == "adult"){
				sampleVarLens = adult.first;
            	sampleVarBoundaries = adult.second;
			} else if (expName == "recidivism"){
				sampleVarLens = recid.first;
            	sampleVarBoundaries = recid.second;
			} else if (expName == "lending"){
				sampleVarLens = lend.first;
            	sampleVarBoundaries = lend.second;
			} else{
				std::cout<<"ERROR: Unrecognized experiment/dataset:"<<expName<<". Exiting..\n";
				exit(1);
			}
			assert(sampleVarBoundaries.size()==sampleVarLens.size()+1);
			assert(sampleVarBoundaries[0]==0);
			for( uint64_t i = 1; i<sampleVarBoundaries.size(); i++){
				assert(sampleVarBoundaries[i]==sampleVarLens[i-1]+sampleVarBoundaries[i-1]);
			}
			rb.SeedEngine();
            for (uint64_t varLens: sampleVarLens){
                samplers.push_back(std::uniform_int_distribution<uint64_t>(0,varLens-1));
            }
		}
		vector<bool> getSample();
        vector<vector<bool>> getSamples(uint64_t N);
		vector<std::uniform_int_distribution<uint64_t>> samplers;
		//uint32_t randClsNum;
	protected:
		vector<uint64_t> sampleVarLens;
        vector<uint64_t> sampleVarBoundaries;
		RandomBits rb;
        CNFFormula F;
		
};
#endif
