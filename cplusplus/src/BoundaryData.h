#ifndef BOUNDARYDATA_H_
#define BOUNDARYDATA_H_

#include <unordered_map>
#include <vector>
#include <cassert>
#include <string>

using std::vector;
using std::string;
class BoundaryInfo{
	public:
		BoundaryInfo(vector<uint64_t> sampleVarLens_,vector<uint64_t> sampleVarBoundaries_): sampleVarLens(sampleVarLens_){
			assert(sampleVarBoundaries_.size()==sampleVarLens.size()+1);
			assert(sampleVarBoundaries_[0]==0);
			sampleVarBoundaries.push_back(0);
			for( uint64_t i = 1; i<sampleVarBoundaries_.size(); i++){
				assert(sampleVarBoundaries_[i]==sampleVarLens_[i-1]+sampleVarBoundaries_[i-1]);
				sampleVarBoundaries.push_back(sampleVarBoundaries_[i]);
			}
		}
	vector<uint64_t> sampleVarLens;
	vector<uint64_t> sampleVarBoundaries;
};
BoundaryInfo adult({{4,9,7,4,9,6,5,2,3,3,3,11},{0,4,13,20,24,33,39,44,46,49,52,55,66}});
BoundaryInfo recid({2,2,2,2,2,2,2,2,2,2,4,4,3,4,4},{0,2,4,6,8,10,12,14,16,18,20,24,28,31,35,39});
BoundaryInfo lend({4,4,4,4,3,4,4,4},{0,4,8,12,16,19,23,27,31,35});

std::unordered_map<string,BoundaryInfo> Info{{"Adult",adult},{"Recidivism",recid},{"Lending",lend}};
#endif
