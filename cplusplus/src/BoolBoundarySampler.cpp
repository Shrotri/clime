#include <iostream>
#include <vector>

#include "BoolBoundarySampler.h"

using std::cout;
using std::endl;

/* We return 0-indexed result (so we deduct 1 from variable number in CNFFormula which starts at 1).
We put value true when literal is positive, and false when literal is negative.
It is the users job to invert it if needed (for example, CMS has literal constructor which takes value true when literal
is negative.)
*/
vector<bool> BoolBoundarySampler::getSample(){
	vector<bool> result(F.sampleVars.size(),false);
	uint32_t sNo = 0;
    for(auto& sampler: samplers){
        uint32_t ind = rb.getRandInt(sampler);
        result[sampleVarBoundaries[sNo]+ind] = true;  
        sNo++;
    }
    for (auto& anchorVar : F.anchorVars){
        result[abs(anchorVar)-1] = (anchorVar > 0);
    }
    // for(auto res: result){
    //     cout<<res<<" ";
    // }
    // cout<<endl;
	return(result);
}

vector<vector<bool>> BoolBoundarySampler::getSamples(uint64_t N){
	vector<vector<bool>> result;
	for (uint64_t i = 0; i<N; i++){
        result.push_back(getSample());
    }
	return(result);
}