#ifndef CNF_FORMULA_H
#define CNF_FORMULA_H

#include <vector>
#include <cstdint>

using std::vector;
class CNFFormula{
	public:
		CNFFormula(uint64_t n_, uint64_t m_, uint64_t minClauseSize_, uint64_t maxClauseSize_, vector<vector<int64_t>> clauses_,vector<uint64_t> sampleVars_, vector<int64_t> anchorVars_, int64_t classVar_)
		: n (n_), m(m_), minClauseSize(minClauseSize_), maxClauseSize(maxClauseSize_), clauses(clauses_), sampleVars(sampleVars_), anchorVars(anchorVars_), classVar(classVar_) {}  
		
		uint64_t n, m, minClauseSize, maxClauseSize;
		vector<vector<int64_t>> clauses;
        vector<uint64_t> sampleVars; //variables to be projected over
        vector<int64_t> anchorVars; //fixed anchor assignments
        int64_t classVar; //var and polarity to check if sample is accepted. Sample is accepted if it is of same polarity.  
};

#endif
