//#include <string>
#include <algorithm>
#include "CNFParser.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <cassert>

using std::ifstream;
using std::cout;
using std::endl;
using std::stoi;
using std::stringstream;
bool less_vectors(const vector<int64_t>& a,const vector<int64_t>& b) {
   return a.size() < b.size();
}

bool less_clauses(const int64_t& a, const int64_t& b){
	return abs(a)<abs(b);
}

bool CNFParser::parse_CNF_dimacs(string file){
	ifstream inputFile;
	inputFile.open(file);
	
	if(!inputFile.is_open()){
		cout<<"Error opening file "<<file<<endl;
		return false;
	}
	
	vector<vector<int64_t>> clauses;
	vector<uint64_t> sampleVars;
	vector<int64_t> anchorVars;
	
	uint64_t n,m,minClauseSize, maxClauseSize;
	string line;
	bool flag=false;
	int i = 0;
	vector<int64_t> a(0);
	while(getline(inputFile,line)){
		
		if(line[0]=='c'){
			if ((line[1] == ' ') && (line[2]=='i') && (line[3] == 'n') && (line[4] == 'd')){
				char *end;
				uint64_t sVar = strtol(&line[0]+6,&end,10);
				cout<<"Sample vars are "<<sVar<<" ";
				while (sVar != 0){
					sampleVars.push_back(sVar);
					cout<<sVar<<" "<<std::flush;
					sVar = strtol(end,&end,10);
				}
				cout<<endl;
				continue;
			}
			else if ((line[1]==' ') && (line[2] == 'c') && (line[3] == 'v') && (line[4] == 'a') && (line[5] == 'r')){
				char *end;
				classVar = strtol(&line[0]+6,&end,10);
				cout<<"Classvar found in dimacs file comment is "<<classVar<<"\n";
				continue;
			}
			else{
				continue;
			}
		}
		if(line[0]=='p'){
			if(flag){
				continue;
			}
			char * end;
			n = strtol(line.c_str()+6,&end,10);	
			m = strtol(end, NULL, 10);
			flag=true;
			
			clauses.push_back(a);
			continue;
		}
		
		i++;
		if (i>m){
			cout<<"Number of clauses: "<<m<<" Number of lines: "<<n<<endl;
			return(false);
		}
		vector<int64_t> b(0);
		clauses.push_back(b);
		stringstream sline(line);
		int64_t lit;
		while (sline>>lit){
			if(lit==0){
				break;
			}
			clauses[i].push_back(lit);
		}
		if (clauses[i].size()==1){
			if(std::find(sampleVars.begin(), sampleVars.end(), abs(clauses[i][0])) != sampleVars.end()){
				anchorVars.push_back(clauses[i][0]);
			}
			else if(abs(clauses[i][0])==abs(classVar)){
				classVar = clauses[i][0];
			}
		}
		sort(clauses[i].begin(), clauses[i].end(),less_clauses);
	}
	if (i<m){
		cout<<"Number of clauses: "<<m<<" Number of lines: "<<n<<endl;
		return(false);
	}
	
	sort(clauses.begin(),clauses.end(),less_vectors);
	minClauseSize = clauses[1].size();
	maxClauseSize = clauses[clauses.size()-1].size();
	inputFile.close();
	cout<<"Anchor bindings are "<<endl;
	for(uint32_t j = 0; j<anchorVars.size();j++){
		cout<<anchorVars[j]<<" ";
	}
	if (classVar == 0){
		cout<<"ERROR: ClassVar not found in comment in DIMACS file!. Define it as 'c cvar 20'. Exiting..\n";
		exit(1);
	}
	cout<<endl<<"ClassVar (after possibly adjusting polarity) is "<<classVar<<endl;
	F = new CNFFormula(n,m,minClauseSize,maxClauseSize,clauses,sampleVars,anchorVars,classVar);
	
	return true;
}

CNFFormula CNFParser::getF(){
	return *F;
}
