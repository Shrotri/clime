#ifndef ESTIMATOR_H_
#define ESTIMATOR_H_

#include <cmath>
#include <vector>
#include <iostream>


using std::vector;
struct Estimate{
	Estimate(double_t num_, uint64_t den_): num(num_),den(den_){}
	double_t num;
	uint64_t den;
};

class Estimator{
	public:
		Estimator(bool approxSampler_):
		approxSampler(approxSampler_), count(-1,1){}
		Estimate& computeEstimate(double_t eps_approx, double_t eps_thresh, double_t delta_, double_t gamma_);
	protected:
		int AAP(double_t eps1, double_t eps_Sampler, double_t delta);
		bool checkThreshold(double_t eps_Sampler, double_t eta, double_t delta, double_t gamma);	
		
		/*Overrider's responsibility to ensure that 0 <= sample_value <= 1 required for correctness of algorithm*/
		virtual vector<double_t> generateAndCheckSamples(uint64_t n_samples, double_t eps_2) = 0;
		
		bool approxSampler;
		Estimate count;
};

#endif
