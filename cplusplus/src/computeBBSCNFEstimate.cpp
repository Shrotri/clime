
#include <vector>
#include "BBSCNFEstimator.h"
#include "CNFParser.h"
#include "Timers.h"

using std::cout;
using std::endl;
using std::vector;
using std::string;

vector<string> split(string inp, string delim){
	vector<string> res;

    auto start = 0U;
    auto end = inp.find(delim);
    while (end != std::string::npos)
    {
        res.push_back(inp.substr(start, end - start));
        start = end + delim.length();
        end = inp.find(delim, start);
    }
	res.push_back(inp.substr(start, end-start));
	return res;
}
 
int main(int argc, char* argv[]){
	cout<< "Starting computeBBSCNFEstimate at ";
	print_wall_time();
	double_t startTime = cpuTimeTotal();
	cout<<endl;
	
	if (!(argc==7)){
		cout<<"Usage: computeCNFEstimate <eps_approx> <eps_thresh> <delta> <gamma> <expName> <input_cnf_dimacs_file>"<<endl;
		exit(1);
	}
	double_t eps_approx = strtod(argv[1],NULL);
	double_t eps_thresh = strtod(argv[2],NULL);
	double_t delta = strtod(argv[3],NULL);
	double_t gamma = strtod(argv[4],NULL);
	std::string expName = std::string(argv[5]);
	std::string cnfFile = std::string(argv[6]);
	
	cout<<"computeBBSCNFEstimate invoked with\n";
	cout<<"epsilon_approx="<<eps_approx<<" epsilon_thresh="<<eps_thresh<<" delta="<<delta<<" gamma="<<gamma<<endl;
	cout<<"cnfFile="<<cnfFile<<endl;
	if (expName=="-"){
		cout<<"expName is -. Extracting expName from cnfFile..\n";
		auto ss = split(cnfFile,"/");
		auto& cnfName = ss[ss.size()-1];
		expName = split(cnfName,"-")[0];
	}
	cout<<"expName:"<<expName<<endl<<endl;
	CNFParser P;
	if(!P.parse_CNF_dimacs(cnfFile)){
		cout<<"Could not parse dimacs file "<<cnfFile<<endl;
		exit(1);
	}
	BBSCNFEstimator D(P.getF(),expName);
	Estimate e = D.computeEstimate(eps_approx,eps_thresh,delta,gamma);
	cout<<endl<<"Estimate is: "<<e.num/e.den<<endl;
	cout<<endl<<endl<< "computeBBSCNFEstimate ended at ";
	print_wall_time();
	cout<<endl;
	double_t timeTaken = cpuTimeTotal() - startTime;
	cout<<"Total Time Taken:"<<timeTaken<<endl;
	return 0;
	
}	
