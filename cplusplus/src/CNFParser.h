#ifndef CNFPARSER_H_
#define CNFPARSER_H_

#include <vector>
#include <string>
#include <cmath>
#include <cassert>
#include <iostream>

#include "CNFFormula.h"

using std::string;
using std::vector;

class CNFParser{
public:
	CNFParser(int64_t classVar_): classVar(classVar_){
        assert(classVar!=0);
    }
    CNFParser(){
        std::cout<<"Expecting classVar to be defined in comment in dimacs file\n";
    }
	bool parse_CNF_dimacs(string file);
	
	CNFFormula getF();
	uint32_t getBitLen();
	
private:
	int64_t classVar = 0;
	CNFFormula *F;
};

#endif
