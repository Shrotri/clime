#include "BBSCNFEstimator.h"
#include "Timers.h"

using std::cout;
using std::endl;

vector<double_t> BBSCNFEstimator::generateAndCheckSamples(uint64_t N, double_t eps2){
	vector<double_t> result;
	vector<vector<bool>> samples = BBS.getSamples(N);
	// uint32_t numTrue = 0;
	for (auto& sample: samples){
		// uint8_t sol = Checker.checkSample(sample);
		// numTrue += sol;
		// result.push_back(sol);
		result.push_back(Checker.checkSample(sample));
	}
	// cout<<numTrue<<" out of "<<N<<" true\n";
	return(result);
}