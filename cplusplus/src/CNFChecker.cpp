#include "CNFChecker.h"

using std::cout;
using std::endl;

/* CMS uses 0 indexing for its variables. The second argument of CMS::Lit is 'is_inverted'. So it must be true if its a 
negative literal and false o/w. We assume that the Boolean vector result has value true at position 'i' if the i+1th literal
in the CNF is true/positive. That is why we put false as the second argument when s[i] is true
*/

bool CNFChecker::checkSample(vector<bool> s){
	vector<CMSat::Lit> assumps;
	for (uint32_t i = 0 ;i<s.size();i++){        //Currently assuming sampleVars have index 1 to some k. If other indices, modify code
		if (s[i]){
			assumps.push_back(CMSat::Lit(i,false));
		}
		else{
			assumps.push_back(CMSat::Lit(i,true));
		}
	}
	CMSat::lbool ret = solver.solve(&assumps);
	if (ret!=CMSat::l_True){
		return false;
	}
	else{
		return true;
	}
}

//See above for why the second argument is true when the literal is inverted and why we deduct 1 from the variable number
bool CNFChecker::initCMS(){
	solver.new_vars(F.n);
	vector<CMSat::Lit> cls;
	bool skipFirst = true;  // First clause is a dummy (empty) clause to make clause numbering start from 1
	for(vector<int64_t> clause: F.clauses){
		if (skipFirst){
			skipFirst = false;
			continue;
		}
		for (auto& vr : clause){
			cls.push_back(CMSat::Lit(abs(vr)-1,vr<0));
		}
		solver.add_clause(cls);
		cls.clear();
	}
	//cls.push_back(CMSat::Lit(abs(F.classVar)-1,F.classVar<0)); // no need as that clause is already part of CNF
	//solver.add_clause(cls);
	CMSat::lbool ret = solver.solve();
	if (ret!=CMSat::l_True){
		cout<<"Formula is UNSAT"<<endl;
		return false;
	}
	else{
		cout<<"Formula is SAT\n";
		return true;
	}
	return true;
}