# CLIME

CLIME is a "linear post-hoc" explainer for explaining Machine Learning models in a black box, model-agnostic way. The key features that set CLIME apart from other explainers are:
 - the ability to incorporate user-defined constraints (as Boolean formulae) over the feature space into the explanation procedure.
 - a precise and theoretically grounded framework for computing the quality of an explanation in terms of metrics like 'fidelity'.

Constraints allow the user to tailor the explanation to their needs, for e.g, by encoding domain knowledge, by zooming in to a particular part of the feature space, and iteratively refining the explanation guided by the fidelity.

The explanation quality, as measured by fidelity, reveals how closely the behavior of the explainer model mimics the behavior of the input classifier in the neighborhood defined by the constraints. In particular, the fidelity value is the fraction of instances in the constrained neighborhood where the output label of the explainer model and input classifier coincide. 

CLIME is based on the [LIME]((https://github.com/marcotcr/lime)) explainer. Lime is based on the work presented in [this paper](https://arxiv.org/abs/1602.04938). Familiarity with the LIME API and usage will be helpful with CLIME as well.

The full preprint (including Appendix) of the CLIME paper is in the file aaai22_preprint.pdf in the root directory. Note that this is just the preprint -- the camera-ready official version will be availalbe on the conference website after the conference.
## Installation

Clone this repository and run:

```sh
pip install .
```

CLIME supports Python 3.5 and later. It has been tested with Python 3.7 on Ubuntu-16.

A Constraint Sampler (eg. [WAPS](https://github.com/meelgroup/WAPS), [UniGen](https://github.com/meelgroup/unigen)) needs to be installed separately. Depending on the sampler used, the following environment variables need to be set

```sh
export WAPS=/path/to/directory/containing waps.py/
```

OR
```sh
export UNIGEN3=/path/to/unigen/binary
```

Additionally, the environment variable $TMP must be set to a directory with enough free space to store temporary intermediate files. For example,

```sh
export TMP=/tmp/
```
## Constraints and Constraint Sampling

CLIME accepts user constraints over the feature space as Boolean formulas. These formulas can be encoded either as nested Python lists of strings, or directly as CNF formulas using the library [PySAT](https://github.com/pysathq/pysat). The former is a convenient API for mapping between features and vairables in the constraints. Further, arbitrary Boolean formulas (not necessarily CNF) can be input this way (the conversion to CNF is handled internally). See the method explain_instance_with_constraints in clime_tabular.py (and similarly in clime_image.py, clime_text.py) for further details.

## Example Usage

See under examples sub-directory. Additional packages like scikit-learn and pandas maybe needed to run examples.

## Authors

Aditya Shrotri, Nina Narodytska.
Based on the paper<sup>*</sup>:
```
Shrotri, A. A., Narodytska, N., Ignatiev, A., Marques-Silva, J., Meel, K. S., & Vardi, M. (AAAI 2022 pre-print). Constraint-Driven Explanations of Black-Box ML Models.
```
Based on [LIME](https://github.com/marcotcr/lime) explainer by Marco Tulio Ribeiro

<sup>*</sup>Final citation will be available after conference proceedings are published