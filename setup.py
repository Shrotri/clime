from setuptools import setup, find_packages

setup(name='clime-exp',
      version='0.2.0.0',
      description='Constrained Local Interpretable Model-Agnostic Explanations for machine learning classifiers',
      url='http://github.com/Shrotri/clime',
      author='Aditya A. Shrotri, Nina Narodytska',
      author_email='aditya.a.shrotri@gmail.com',
      license='BSD',
      packages=find_packages(exclude=['js', 'node_modules', 'tests']),
      python_requires='>=3.5',
      install_requires=[
          'matplotlib',
          'numpy',
          'scipy',
          'tqdm',
          'pillow>=5.4.1',
          'scikit-learn>=0.18',
          'scikit-image>=0.12',
          'setuptools>=46.0.0',
          'python-sat'
      ],
      extras_require={
          'dev': ['pytest', 'flake8'],
      },
      include_package_data=True,
      zip_safe=False)
